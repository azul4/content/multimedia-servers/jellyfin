# jellyfin

The Free Software Media System

https://github.com/jellyfin/jellyfin

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/multimedia-servers/jellyfin.git
```
<br>

**Provides:**

jellyfin

jellyfin-server

jellyfin-web
